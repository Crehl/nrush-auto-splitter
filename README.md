Nitronic Rush Auto Splitter v0.1.3
==================================

A tool for automatically splitting timers (e.g. WSplit) while speedrunning Nitronic Rush.

As of v0.1.2, the auto splitter now comes in two different forms (you only need to use one):

* A stand-alone tool which functions the same as versions before v0.1.2 (nrush-auto-splitter.exe)
* A LiveSplit component which works like a plugin (NRushAutoSplitter.dll)

If you're using LiveSplit, it is recommended that you use the component.
However, the stand-alone will still work fine with both it and WSplit.

Usage
-----
In order for things to work properly, a small configuration change needs to be made to one of NR's config files.
Find the file "Nitronic Rush\config\nitronic.ini" (e.g. "C:\Program Files (x86)\digipen\Nitronic Rush\config\nitronic.ini"),
and edit the "CreateConsole" line under "[Dev]" to read "CreateConsole=true" (this only needs to be done once).

If you're using the stand-alone, just run your timer and then the auto splitter, which will open NR automatically.

If you're using the LiveSplit component, you'll need to perform a couple of steps first (these only need to be done once):

* Copy NRushAutoSplitter.dll into your LiveSplit\Components folder
* Restart LiveSplit if it's already running
* Right click LiveSplit -> Edit Layout -> Add the "Nitronic Rush Auto Splitter" component (this won't be visible, don't worry)

At this point you can right click LiveSplit, and select "Launch Nitronic Rush" under the Control menu to start NR.

Note that in both cases the auto splitter needs to control how NR starts in order to work - if you start NR yourself the auto splitter won't function.

Supported Timers
----------------
LiveSplit (Tested with v1.2 - v1.4)
WSplit (Tested with v1.4.4 and v1.5.0)

Known Issues (Both versions)
----------------------------
* Doesn't work with challenge levels, except for splitting on finish (will have to be started manually).

Known Issues (Stand-alone version only)
---------------------------------------
* Changing the split or reset hotkey in LiveSplit requires restarting both LiveSplit and the auto splitter to update everything.
* Not compatible with certain split/reset hotkeys in LiveSplit (for example, gamepad/controller hotkeys won't work).
* Doesn't work with LiveSplit if Global Hotkeys isn't on, but will work with WSplit in the same situation.

Contact
-------
'Crehl' on irc.speedrunslive.com #nrush  
or send me a message on Twitch: http://twitch.tv/Crehl

https://bitbucket.org/Crehl/nrush-auto-splitter