﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32;
using WindowsInput;
using WindowsInput.Native;

namespace NRushAutoSplitter
{
    class Program
    {
        static List<Timer> timers = new List<Timer>(){
            new LiveSplit(),
            new WSplit(),
        };

        static Timer currentTimer;
        static AutoSplitter autoSplitter = new AutoSplitter();

        static void Main(string[] args)
        {
            printHeader();

            if (!autoSplitter.FindFilePath(args))
            {
                Console.WriteLine("Unable to find Nitronic Rush executable.");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }

            while (!autoSplitter.CheckConfig())
            {
                Console.WriteLine("CreateConsole=true has not been set in {0}.", autoSplitter.ConfigPath);
                Console.WriteLine("Please set this, then press any key to continue...\n");

                if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                    return;
            }

            autoSplitter.OnStart += split;
            autoSplitter.OnSplit += split;
            autoSplitter.OnReset += reset;
#if DEBUG
            autoSplitter.OnStdOut += output;
            autoSplitter.OnStdErr += output;
#endif

            Process p = autoSplitter.LaunchGame();
            Console.WriteLine("Starting {0}", p.StartInfo.FileName);
            p.WaitForExit();
        }

        private static void printHeader()
        {
            Version v = Assembly.GetExecutingAssembly().GetName().Version;
            string s = String.Format("Nitronic Rush Auto Splitter v{0}", v);
            Console.WriteLine(s);
            Console.WriteLine(new String('=', s.Length));
        }

        static void output(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null || e.Data == "device is LOST! waiting...")
                return;

            if (e.Data.Contains("START GAME, NO TUT!!"))
                Console.ForegroundColor = ConsoleColor.Yellow;
            else if (e.Data.Contains("> PostBaseLevelEnter()"))
                Console.ForegroundColor = ConsoleColor.Green;
            else if (e.Data.Contains("Finish!!"))
                Console.ForegroundColor = ConsoleColor.Magenta;
            else
                Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine(e.Data);
        }

        static bool checkCurrentTimer()
        {
            try
            {
                currentTimer = timers.Where(t => t.IsRunning()).First();
                if (!currentTimer.IsConfigured)
                    currentTimer.Configure();

                return true;
            }
            catch (Exception)
            {
                currentTimer = null;
                Console.Error.WriteLine("Err: No timer found.");
                return false;
            }
        }

        static void split(object sender, EventArgs e)
        {
            if (checkCurrentTimer())
            {
                currentTimer.Split();
                Console.WriteLine("{0}: Split", currentTimer.WindowTitle);
            }
        }

        static void reset(object sender, EventArgs e)
        {
            if (checkCurrentTimer())
            {
                currentTimer.Reset();
                Console.WriteLine("{0}: Reset", currentTimer.WindowTitle);
            }
        }
    }
}
