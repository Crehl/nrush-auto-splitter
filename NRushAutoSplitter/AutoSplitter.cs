﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace NRushAutoSplitter
{
    public class AutoSplitter
    {
        const string REG_PATH = @"SOFTWARE\DigiPen\Nitronic Rush\Settings";
        const string DEFAULT_EXE = "NitronicRush.exe";
        const string CONFIG_DIR = "config";
        const string CONFIG_NITRONIC = "nitronic.ini";
        
        bool running = false;

        public string InstallPath { get; set; }
        public string Exe { get; set; }
        public string ConfigPath
        {
            get { return Path.Combine(InstallPath, CONFIG_DIR, CONFIG_NITRONIC); }
        }

        public event EventHandler OnStart;
        public event EventHandler OnSplit;
        public event EventHandler OnReset;
        public event DataReceivedEventHandler OnStdOut;
        public event DataReceivedEventHandler OnStdErr;

        public AutoSplitter()
        {

        }

        public Process LaunchGame()
        {
            Process p = new Process();
            p.StartInfo.FileName = Path.Combine(InstallPath, Exe);
            p.StartInfo.WorkingDirectory = InstallPath;
            p.StartInfo.UseShellExecute = false;

            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardInput = true;
            p.OutputDataReceived += (sender, e) => output(sender, e, true);
            p.ErrorDataReceived += (sender, e) => output(sender, e, false);

            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();

            return p;
        }

        public bool FindFilePath(string[] args)
        {
            return filePathFromArgs(args) || filePathFromCwd() || filePathFromRegistry();
        }

        bool filePathFromArgs(string[] args)
        {
            if (args == null || args.Length == 0)
                return false;

            try
            {
                InstallPath = Path.GetDirectoryName(args[0]);
                Exe = Path.GetFileName(args[0]);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        bool filePathFromCwd()
        {
            string file = Path.Combine(Environment.CurrentDirectory, DEFAULT_EXE);
            if (!File.Exists(file))
                return false;

            InstallPath = Environment.CurrentDirectory;
            Exe = DEFAULT_EXE;
            return true;
        }

        bool filePathFromRegistry()
        {
            try
            {
                var baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
                var subKey = baseKey.OpenSubKey(REG_PATH);
                InstallPath = (string)subKey.GetValue("InstallPath");
                Exe = DEFAULT_EXE;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckConfig()
        {
            foreach (string line in File.ReadAllLines(ConfigPath))
            {
                if (line.Replace(" ", string.Empty).ToLower() == "createconsole=true")
                    return true;
            }
            return false;
        }

        void output(object sender, DataReceivedEventArgs e, bool stdout)
        {
            if (e.Data == null)
                return;
            
            if (e.Data.Contains("START GAME, NO TUT!!"))
            {
                reset();
                start();
                running = true;
            }
            else if (e.Data.Contains("> PostBaseLevelEnter()"))
            {
                if (!running)
                {
                    reset();
                    start();
                    running = true;
                }
            }
            else if (e.Data.Contains("Finish!!"))
            {
                split();
            }
            else if (e.Data.Contains("UpdateLevelSelectMenu"))
            {
                running = false;
            }

            if (stdout)
            {
                if (OnStdOut != null)
                    OnStdOut.Invoke(this, e);
            }
            else
            {
                if (OnStdErr != null)
                    OnStdErr.Invoke(this, e);
            }
        }

        void reset()
        {
            if (OnReset != null)
                OnReset.Invoke(this, EventArgs.Empty);
        }

        void split()
        {
            if (OnSplit != null)
                OnSplit.Invoke(this, EventArgs.Empty);
        }

        void start()
        {
            if (OnStart != null)
                OnStart.Invoke(this, EventArgs.Empty);
        }
    }
}
