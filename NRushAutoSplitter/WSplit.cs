﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NRushAutoSplitter
{
    class WSplit : Timer
    {
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        private const uint HOTKEY_RESET = 0x9D84;
        private const uint HOTKEY_SPLIT = 0x9D82;
        private const uint WM_HOTKEY = 0x312;
        private const string WSPLIT_TITLE = "WSplit";

        public WSplit() : base("WSplit")
        {
        }

        public override void Reset()
        {
            sendHotkey(HOTKEY_RESET);
        }

        public override void Split()
        {
            sendHotkey(HOTKEY_SPLIT);
        }

        private void sendHotkey(uint hotkey)
        {
            IntPtr hWnd = FindWindow(null, WSPLIT_TITLE);
            if (hWnd != IntPtr.Zero)
                PostMessage(hWnd, WM_HOTKEY, (IntPtr)hotkey, IntPtr.Zero);
        }
    }
}
