﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WindowsInput;
using WindowsInput.Native;

namespace NRushAutoSplitter
{
    class LiveSplit : Timer
    {
        private const string SETTINGS_FILE = "settings.cfg";
        private Keys splitKey, resetKey;
        private IKeyboardSimulator keySim;

        public LiveSplit() : base("LiveSplit")
        {
            keySim = new InputSimulator().Keyboard;
        }

        public override void Configure()
        {
            Process lsp = Process.GetProcessesByName("LiveSplit")[0];
            string path = Path.Combine(Path.GetDirectoryName(lsp.Modules[0].FileName), SETTINGS_FILE);

            using(XmlReader r = XmlReader.Create(path))
            {
                while (!r.EOF)
                {
                    while (((r.Name != "SplitKey" && r.Name != "ResetKey") || r.NodeType != XmlNodeType.Element) && !r.EOF)
                        r.Read();

                    switch (r.Name)
                    {
                        case "SplitKey":
                            splitKey = readKey(r);
                            break;
                        case "ResetKey":
                            resetKey = readKey(r);
                            break;
                    }
                }
            }

            IsConfigured = true;
        }

        private Keys readKey(XmlReader r)
        {
            r.Read();
            Keys k = parseKey(r.Value);
            r.Read();
            r.Read();
            return k;
        }

        private Keys parseKey(string key)
        {
            Keys k;
            if (!Enum.TryParse<Keys>(key, out k))
            {
                Console.Error.WriteLine("Err: Couldn't parse LiveSplit key '{0}'", key);
                return Keys.None;
            }

            return k;
        }

        public override void Reset()
        {
            if (resetKey != Keys.None)
                keySim.KeyPress((VirtualKeyCode)resetKey);
            else
                Console.Error.WriteLine("Err: LiveSplit reset key is None");
        }

        public override void Split()
        {
            if (splitKey != Keys.None)
                keySim.KeyPress((VirtualKeyCode)splitKey);
            else
                Console.Error.WriteLine("Err: LiveSplit split key is None");
        }
    }
}
