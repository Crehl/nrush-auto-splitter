﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NRushAutoSplitter
{
    abstract class Timer
    {
        [DllImport("user32.dll", SetLastError = true)]
        protected static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        public bool IsConfigured { get; protected set; }
        public string WindowTitle { get; protected set; }

        public Timer(string title)
        {
            IsConfigured = false;
            WindowTitle = title;
        }

        public virtual bool IsRunning()
        {
            return FindWindow(null, WindowTitle) != IntPtr.Zero;
        }

        public virtual void Configure()
        {
            IsConfigured = true;
        }

        public abstract void Reset();
        public abstract void Split();
    }
}
