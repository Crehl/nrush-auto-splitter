﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using LiveSplit.Model;
using NRushAutoSplitter;

namespace LiveSplit.UI.Components
{
    public class NRushAutoSplitterComponent : IComponent
    {
        public string ComponentName { get { return "Nitronic Rush Auto Splitter"; } }
        public IDictionary<string, Action> ContextMenuControls { get; protected set; }
        public NRushAutoSplitterSettings Settings { get; set; }
        public float HorizontalWidth { get { return 0f; } }
        public float MinimumHeight { get { return 0f; } }
        public float MinimumWidth { get { return 0f; } }
        public float PaddingBottom { get { return 0f; } }
        public float PaddingLeft { get { return 0f; } }
        public float PaddingRight { get { return 0f; } }
        public float PaddingTop { get { return 0f; } }
        public float VerticalHeight { get { return 0f; } }

        NRushAutoSplitter.AutoSplitter autoSplitter;
        LiveSplitState lsState;
        ITimerModel timerModel = new TimerModel();

        public NRushAutoSplitterComponent(LiveSplitState state)
        {
            lsState = state;
            timerModel.CurrentState = state;

            ContextMenuControls = new Dictionary<string, Action>()
            {
                { "Launch Nitronic Rush", new Action(LaunchGame) }
            };

            Settings = new NRushAutoSplitterSettings();

            autoSplitter = new NRushAutoSplitter.AutoSplitter();
            autoSplitter.OnStart += (sender, e) => start();
            autoSplitter.OnSplit += (sender, e) => split();
            autoSplitter.OnReset += (sender, e) => reset();
        }

        bool checkGlobalHotkeys()
        {
            return !this.Settings.ObeyGlobalHotkeys || lsState.Settings.GlobalHotkeysEnabled;
        }

        void start()
        {
            if (!checkGlobalHotkeys())
                return;

            bool doubleTap = lsState.Settings.DoubleTapPrevention;
            lsState.Settings.DoubleTapPrevention = false;
            timerModel.Start();
            lsState.Settings.DoubleTapPrevention = doubleTap;
        }

        void split()
        {
            if (!checkGlobalHotkeys())
                return;

            timerModel.Split();
        }

        void reset()
        {
            if (!checkGlobalHotkeys())
                return;

            timerModel.Reset();
        }

        public void LaunchGame()
        {
            if (!autoSplitter.FindFilePath(null))
            {
                MessageBox.Show("Unable to find Nitronic Rush executable.", "NRush Auto Splitter Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!autoSplitter.CheckConfig())
            {
                MessageBox.Show(string.Format("CreateConsole=true has not been set in {0}.", autoSplitter.ConfigPath), "NRush Auto Splitter Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            autoSplitter.LaunchGame();
        }

        public void DrawHorizontal(Graphics g, LiveSplitState state, float height, Region clipRegion)
        {
        }

        public void DrawVertical(Graphics g, LiveSplitState state, float width, Region clipRegion)
        {
        }

        public XmlNode GetSettings(XmlDocument document)
        {
            return Settings.GetSettings(document);
        }

        public Control GetSettingsControl(LayoutMode mode)
        {
            return Settings;
        }

        public void RenameComparison(string oldName, string newName)
        {
        }

        public void SetSettings(XmlNode settings)
        {
            Settings.SetSettings(settings);
        }

        public void Update(IInvalidator invalidator, LiveSplitState state, float width, float height, LayoutMode mode)
        {
        }

        public void Dispose()
        {
        }
    }
}
