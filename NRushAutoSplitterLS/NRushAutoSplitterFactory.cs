﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LiveSplit.UI.Components;
using LiveSplit.Model;
using UpdateManager;

[assembly: ComponentFactory(typeof(NRushAutoSplitterFactory))]

namespace LiveSplit.UI.Components
{
    class NRushAutoSplitterFactory : IComponentFactory, IUpdateable
    {
        public string ComponentName { get { return "Nitronic Rush Auto Splitter"; } }

        public IComponent Create(LiveSplitState state)
        {
            return (IComponent)new NRushAutoSplitterComponent(state);
        }

        public string UpdateName { get { return ComponentName; } }
        public string UpdateURL { get { return "http://crehl.co.uk/livesplit/"; } }
        public Version Version { get { return Version.Parse("0.1.3"); } }
        public string XMLURL { get { return "http://crehl.co.uk/livesplit/components/update.NRushAutoSplitter.xml"; } }

        public ComponentCategory Category
        {
            get { return ComponentCategory.Other; }
        }

        public string Description
        {
            get { return "Nitronic Rush Auto Splitter"; }
        }
    }
}
