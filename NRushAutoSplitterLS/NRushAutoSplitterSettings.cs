﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LiveSplit.UI.Components
{
    public partial class NRushAutoSplitterSettings : UserControl
    {
        public enum Mode
        {
            Auto,
            AllLevels,
            StoryHardcore,
        };

        static Dictionary<string, Mode> descriptions = new Dictionary<string, Mode>()
        {
            {"Auto (Split every level)", Mode.Auto},
            {"All Levels (Split every levelset)", Mode.AllLevels},
            {"Story + Hardcore (Split every levelset)", Mode.StoryHardcore},
        };

        public bool ObeyGlobalHotkeys { get; set; }
        public Mode SplitMode { get; set; }

        public NRushAutoSplitterSettings()
        {
            InitializeComponent();

            foreach (string desc in descriptions.Keys)
                cmbMode.Items.Add(desc);
            cmbMode.SelectedIndex = 0;

            chkObeyGlobalHotkeys.CheckedChanged += chkObeyGlobalHotkeys_CheckedChanged;
            cmbMode.SelectedIndexChanged += cmbMode_SelectedIndexChanged;
        }

        void cmbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            SplitMode = descriptions[cmbMode.SelectedItem.ToString()];
        }

        void chkObeyGlobalHotkeys_CheckedChanged(object sender, EventArgs e)
        {
            ObeyGlobalHotkeys = chkObeyGlobalHotkeys.Checked;
        }

        public XmlNode GetSettings(XmlDocument document)
        {
            XmlElement element = document.CreateElement("Settings");

            var e = document.CreateElement("Version");
            e.InnerText = "0.1.2";
            element.AppendChild(e);

            e = document.CreateElement("ObeyGlobalHotkeys");
            e.InnerText = ObeyGlobalHotkeys.ToString();
            element.AppendChild(e);

            e = document.CreateElement("SplitMode");
            e.InnerText = SplitMode.ToString();
            element.AppendChild(e);

            return element;
        }

        public void SetSettings(XmlNode settings)
        {
            XmlElement element = (XmlElement)settings;
            if (element["Version"] != null)
            {
                ObeyGlobalHotkeys = bool.Parse(element["ObeyGlobalHotkeys"].InnerText);
                SplitMode = (Mode)Enum.Parse(typeof(Mode), element["SplitMode"].InnerText);
            }

            chkObeyGlobalHotkeys.Checked = ObeyGlobalHotkeys;
            cmbMode.SelectedItem = descriptions.First(kvp => kvp.Value == SplitMode).Key;
        }
    }
}
